import java.util.Scanner;

public class taxCalculation {
	public static void main(String[] args) {
 
        Scanner input = new Scanner(System.in);
        
        double tax;
        double salary = input.nextDouble();
        
        if(salary >= 0.00 && salary <= 2000.00) {
        	System.out.print("Isento");
        }
        else {
        	if(salary >= 2000.01 && salary<= 3000.00) {
        	  salary = salary - 2000;
        		 tax = (salary*8)/100;
        	 }
        	 else if(salary >= 3000.01 && salary <= 4500.00){
        	  		double A = (1000*8)/100;
        	  		salary = salary - 3000;
        	  		double B = (salary*18)/100;
        	  		tax = A+B;
        	  	}
        	 else{
     	  		double A = (1000*8)/100;
     	  		double B = (1500*18)/100;
     	  		salary = salary - 4500;
     	  		double C = (salary*28)/100;
     	  		tax = A+B+C;
        	}
        	System.out.print("R$ "+String.format("%.2f", tax));
        }
 
    }
 
}
